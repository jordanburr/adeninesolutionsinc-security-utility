require('dotenv').config();
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const sql = require("mssql");
const crypto = require("crypto");
const { v4: uuidv4 } = require("uuid");

const PORT = process.env['PORT'];
const apiPrefix = '/api/user-security';

// config for your database
const config = {
  user: process.env['DBUSER'],
  password: process.env['DBPASSWORD'],
  server: process.env['DBSERVER'],
  database: process.env['DBDATABASE'],
  options: {
    encrypt: true,
    enableArithAbort: true,
  },
};


let router = express.Router();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use(apiPrefix, router);
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}...`);
});

//check user token
router.use((req, res, next) => {
  // skip middleware if post (login)
  if (req.method === "POST") {
    next();
  } else {
    const token = req.headers.token;
    getUserByToken(token.toLowerCase()).then(
      (result) => {
        const user =
          result.recordset[0] === undefined ? null : result.recordset[0];

        const currentToken = user ? user.CurrentToken : "";
        if (token.toUpperCase() === currentToken) {
          req.user = user;
          next();
        } else {
          res.status(404).send("Invalid token");
        }
      },
      (err) => {
        res.send(err);
      }
    );
  }
});

// login and send token to user
router.post('/login', (req, res) => {
  let valid = false;
  const username = req.body.username;
  const password = req.body.password;

  sql.connect(config, (err) => {
    if (err) reject(err);

    sql.query(
      `select *
          from [User]
          where Username = '${username}' or Email = '${username}' and Deleted = 0`,
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          result.recordset.forEach((user) => {
            // if plain text
            if (user.Password === password) {
              const salt = crypto.randomBytes(8).toString("base64");

              const hashedPassword = crypto
                .createHash("sha1")
                .update(password + salt)
                .digest("hex");

              const request = new sql.Request();
              request.input("PasswordFormatId", 1);
              request.input("Password", hashedPassword);
              request.input("PasswordSalt", salt);
              request.query(
                `update [User] set PasswordFormatId = @PasswordFormatId, Password = @Password, PasswordSalt = @PasswordSalt where Id = ${user.Id}`
              );
            }

            valid = validateUser(password, user.PasswordSalt, user.Password);

            if (valid) {
              const request = new sql.Request();
              request.input("CurrentToken", uuidv4().toUpperCase());
              request.query(
                `update [User] set CurrentToken = @CurrentToken where Id = ${user.Id}`
              );
              request.query(
                `select * from [User] where Id = ${user.Id}`,
                (err, recordset) => {
                  if (err) console.log(err);
                  res.send(recordset.recordset[0]);
                }
              );
            }
          });
          if (!valid) {
            res.status(400).send("Invalid Credentials");
          }
        }
      }
    );
  });
});

// get all users from database
router.get('/users/all', (req, res) => {
  sql.connect(config, (err) => {
    if (err) console.log(err);

    let request = new sql.Request();

    request.query(`select * from [User]`, (err, recordset) => {
      if (err) {
        res.send(err);
        console.log(err);
      } else {
        res.send(recordset.recordset);
      }
    });
  });
});

//get all tenants for user
router.get('/tenants/:id', (req, res) => {
  sql.connect(config, (err) => {
    if (err) console.log(err);

    let request = new sql.Request();

    request.query(
      `SELECT t.id, t.Name, m.UserRole_id, u.Name AS RoleName
    FROM Tenant_User_Map m INNER JOIN Tenant t ON t.id = m.Tenant_id INNER JOIN UserRole u ON u.Id = m.UserRole_id where m.User_id = ${req.params.id}`,
      (err, recordset) => {
        if (err) {
          res.send(err);
        } else {
          res.send(recordset.recordset);
        }
      }
    );
  });
});

// update user role for tenant
router.put('/tenants/:id', (req, res) => {
  sql.connect(config, (err) => {
    if (err) console.log(err);

    let request = new sql.Request();

    request.query(
      `update Tenant_User_Map set UserRole_id = ${req.body.userRoleId} where User_id = ${req.params.id} and Tenant_id = ${req.body.id}`,
      (err, recordset) => {
        if (err) {
          res.send(err);
        } else {
          res.status(200).send('Tenant-User map updated');
        }
      }
    );
  });
});

// get all roles 
router.get('/roles', (req, res) => {
  sql.connect(config, (err) => {
    if (err) console.log(err);

    let request = new sql.Request();

    request.query(`select * from [UserRole]`, (err, recordset) => {
      if (err) {
        res.send(err);
        console.log(err);
      } else {
        res.send(recordset.recordset);
      }
    });
  });
});

const validateUser = (enteredPassword, passwordSalt, realPassword) => {
  const pwd = crypto
    .createHash("sha1")
    .update(enteredPassword + passwordSalt)
    .digest("hex");
  return pwd.toUpperCase() === realPassword || enteredPassword === realPassword;
};

const getUserByToken = async (token) => {
  return await new Promise((resolve, reject) => {
    // check if valid guid
    let regexGuid = /^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$/gi;
    if (regexGuid.test(token)) {
      sql.connect(config, (err) => {
        if (err) reject(err);

        sql.query(
          `select * from [User] where CurrentToken = '${token}'`,
          (err, user) => {
            if (err) {
              reject(err);
            } else {
              resolve(user);
            }
          }
        );
      });
    } else {
      reject("Invalid token format");
    }
  });
};