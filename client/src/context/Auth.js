import React, { createContext, useState } from "react";

export const AuthContext = createContext();

const AuthenticationContextProvider = (props) => {
  const [isAuthenticated, setAuthentication] = useState(false);
  const [user, setUser] = useState({});

  const toggleAuthentication = () => {
    setAuthentication(!isAuthenticated);
  };

  const newUser = (user) => {
    sessionStorage.setItem('token', user.token);
    sessionStorage.setItem('user_id', user.id);
    setUser(user);
  };

  return (
    <AuthContext.Provider
      value={{
        isAuthenticated: isAuthenticated,
        toggleAuthentication: toggleAuthentication,
        user: user,
        newUser: newUser,
        setAuthentication: setAuthentication,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthenticationContextProvider;
