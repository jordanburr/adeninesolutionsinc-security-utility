import React from "react";
import "./App.css";
import UsersPage from "./components/UsersPage/UsersPage";
import LoginPage from './components/Login/LoginPage';
import PageNotFound from './components/PageNotFound';
import GuardedRoute from "./components/GuardedRoute";
import AuthenticationContextProvider, { AuthContext } from "./context/Auth";
import Navbar from "./components/UsersPage/Navbar";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <AuthenticationContextProvider>
        <AuthContext.Consumer>
          {(value) => (value.isAuthenticated ? <Navbar /> : null)}
        </AuthContext.Consumer>
        <Switch>
          <Route path="/" exact component={LoginPage} />
          <GuardedRoute path="/home" exact component={UsersPage} />
          <Route component={PageNotFound} />
        </Switch>
      </AuthenticationContextProvider>
    </Router>
  );
}

export default App;
