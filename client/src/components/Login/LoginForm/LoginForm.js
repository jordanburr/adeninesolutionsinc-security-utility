import React, { useContext, useState, useEffect } from "react";
import { AuthContext } from "../../../context/Auth";
import { useHistory } from "react-router-dom";
import axios from "axios";

const API = "http://localhost:5000/api/user-security/login";

const LoginForm = () => {
  const {
    newUser,
    setAuthentication,
    isAuthenticated,
    toggleAuthentication,
  } = useContext(AuthContext);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [rememberUsername, setRememberUsername] = useState(false);
  const [wrongCredentials, setWrongCredentials] = useState(false);
  const [loading, setLoading] = useState(false);

  let history = useHistory();

  useEffect(() => {
    setAuthentication(isAuthenticated);
    window.onpopstate = () => toggleAuthentication();
  });

  const fetchData = async () => {
    const body = {
      username: username,
      password: password,
    };
    return await axios.post(API, body);
  };

  const handleSubmit = (event) => {
    setLoading(true);
    fetchData()
      .then((res) => {
          const user = res.data;

          toggleAuthentication();
          newUser({
            id: user.Id,
            email: user.Email,
            firstName: user.FirstName,
            lastName: user.astName,
            token: user.CurrentToken,
          });
          history.push("/home");
      })
      .catch(() => {
        setWrongCredentials(true);
        setLoading(false);
        setUsername("");
        setPassword("");
      });
    // toggleAuthentication();
    // history.push('/home');

    event.preventDefault();
  };

  const handleCheckboxChange = (event) => {
    const value = event.target.checked;
    setRememberUsername(value);
  };

  const handleUsernameChange = (event) => {
    const value = event.target.value;
    setUsername(value);
  };

  const handlePasswordChange = (event) => {
    const value = event.target.value;
    setPassword(value);
  };

  const loginTitleStyle = {
    textAlign: "center",
  };

  return (
    <div className="container py-3">
      <h3 style={loginTitleStyle}>Login</h3>
      <div className="row justify-content-center">
        <form>
          {wrongCredentials && (
            <div className="alert alert-danger" role="alert">
              Invalid Credentials
            </div>
          )}
          <div className="form-label-group form-group">
            <input
              type="username"
              id="username"
              name="username"
              className="form-control"
              placeholder="Username"
              value={username}
              onChange={handleUsernameChange}
              required
              autoFocus
            />
          </div>
          <div className="form-label-group form-group">
            <input
              type="password"
              id="password"
              name="password"
              value={password}
              onChange={handlePasswordChange}
              className="form-control"
              placeholder="Password"
              required
            />
          </div>
          {/* <div className="custom-control custom-checkbox mb-3 form-group">
            <input
              type="checkbox"
              className="custom-control-input"
              id="rememberUsername"
              name="rememberUsername"
              onChange={handleCheckboxChange}
            />
            <label className="custom-control-label" htmlFor="rememberUsername">
              Remember password
            </label>
          </div> */}

          <button
            className="btn btn-lg btn-success btn-block font-weight-bold mb-2"
            type="submit"
            onClick={handleSubmit}
          >
            {loading ? (
              <span
                className="spinner-border align-items-center"
                role="status"
                aria-hidden="true"
              ></span>
            ) : (
              <span>Log In</span>
            )}
          </button>

          <div className="text-center">
            <a
              className="small"
              target="_blank"
              rel="noopener noreferrer"
              href="https://www.retainbi.com/Login.aspx#"
            >
              Forgot password?
            </a>
          </div>
        </form>
      </div>
    </div>
  );
};

export default LoginForm;
