import React from "react";
import LoginForm from "./LoginForm/LoginForm";

const LoginPage = () => {
  const websiteTitleStyle = {
    textAlign: "center",
  };
  return (
    <div style={websiteTitleStyle}>
      <div className="container">
        <div className="row py-2">
          <div className="col-sm-6">
            <img
              src={require("../../images/Logo.png")}
              className="img-fluid"
              alt=""
            />
          </div>
          <div className="col-sm-6 align-items-center d-flex">
            <h1 style={{fontFamily: "Century Gothic"}}>RetainBI User Security Management</h1>
          </div>
        </div>
        <div className="row py-3">
          <LoginForm />
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
