import React, { useState, useEffect } from "react";
import UserItem from "./UserItem";
import { Table, FormControl } from "react-bootstrap";
import UserInfo from "./UserInfo";
import axios from "axios";

const API = "http://localhost:5000/api/user-security/users/all";

const UserPage = () => {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [filterInput, setFilterInput] = useState("");
  const [filterType, setFilterType] = useState("Any");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setLoading(true);
    await axios
      .get(API, {
        headers: { token: sessionStorage.getItem("token") },
      })
      .then((res) => {
        setUsers(res.data);
      })
      .catch((error) => console.log(error));
    setLoading(false);
  };

  const handleUserSelect = (user) => {
    window.scrollTo({top: 0, behavior: 'smooth'});
    setSelectedUser(user === selectedUser ? null : user);
  };

  const handleFilterChange = (e) => {
    setFilterInput(e.target.value.toLowerCase());
  };

  const handleFilterTypeChange = (type) => {
    switch (type) {
      case "First Name":
        setFilterType("FirstName");
        break;
      case "Last Name":
        setFilterType("LastName");
        break;
      case "Email":
        setFilterType("Email");
        break;
      default:
        setFilterType("Any");
        break;
    }
  };

  const userItems = users
    .filter((user) =>
      filterType === "Any"
        ? user.FirstName.toLowerCase().includes(filterInput) ||
          user.LastName.toLowerCase().includes(filterInput) ||
          user.Email.toLowerCase().includes(filterInput)
        : user[filterType].toLowerCase().includes(filterInput)
    )
    .sort((a, b) => {
      return a.FirstName.localeCompare(b.FirstName);
    })
    .map((user) => (
      <UserItem
        key={user.Id}
        user={user}
        handleUserSelect={handleUserSelect}
        isSelected={user === selectedUser}
      />
    ));

  return (
    <div className="container-fluid" style={{padding: "0px 150px"}}>
      <div className="row">
        <div className="card col-md">
          <div
            className="card-header py-3 bg-white"
            style={{ textAlign: "center" }}
          >
            <h4 className="m-0 font-weight-bold text-success">Users</h4>
          </div>

          <div className="container mt-3">
            <div className="row">
              <div
                className="col-sm-1 p-0 my-auto float-right"
                style={{ textAlign: "center" }}
              >
                <label className="mb-0">Filter:</label>
              </div>
              <div className="col-sm-4 pl-1">
                <select
                  className="custom-select"
                  id="filterType"
                  variant="outline-secondary"
                  onChange={(e) => handleFilterTypeChange(e.target.value)}
                >
                  <option defaultValue>Any</option>
                  <option>First Name</option>
                  <option>Last Name</option>
                  <option>Email</option>
                </select>
              </div>
              <div className="col-sm-7">
                <FormControl
                  onChange={(e) => handleFilterChange(e)}
                  placeholder="Type to filter..."
                />
              </div>
            </div>
          </div>
          {loading ? (
            <span
              className="spinner-border align-items-center m-3"
              role="status"
              aria-hidden="true"
            ></span>
          ) : (
            <div className="card-body">
              <Table bordered hover size="lg">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    {/* <th>Tenants</th> */}
                    <th>Edit</th>
                  </tr>
                </thead>
                <tbody>{userItems}</tbody>
              </Table>
            </div>
          )}
        </div>
        {selectedUser ? (
          <div className="card col-md-5">
            <div className="card-body">
              <UserInfo user={selectedUser} handleUserSelect={handleUserSelect} />
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default UserPage;
