import React from "react";
import TenantList from "./TenantList";
import { Button, Card } from "react-bootstrap";

// get our fontawesome imports
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const UserInfo = (props) => {
  const { user, handleUserSelect } = props;

  return (

    <Card style={{border: "none"}}>
      <Card.Header className="bg-transparent">
        <div className="container-fluid">
          <div className="row align-items-center d-flex">
            <div className="col">
              <Button
                variant="danger"
                size="sm"
                onClick={() => handleUserSelect(user)}
              >
                <FontAwesomeIcon icon={faArrowLeft} />
              </Button>
            </div>
            <div className="col">
              <h5 className="mb-0">
                {user.FirstName} {user.LastName}
                <br />
                {user.Email}
              </h5>
            </div>
          </div>
        </div>
      </Card.Header>
      <Card.Body>
        <Card.Title>Manage Roles</Card.Title>
        <TenantList userId={user.Id} />
      </Card.Body>
    </Card>
  );
};

export default UserInfo;
