import React from "react";
import { Form, Col, Card } from "react-bootstrap";

const TenantItem = (props) => {
  const { tenant, currentRole, handleRoleChange, roles } = props;

  const roleItems = roles.map((role, index) => (
    <Form.Check
      key={index}
      type="radio"
      label={role.Name}
      name="formHorizontalRadios"
      defaultChecked={currentRole === role.Name}
      onChange={() => handleRoleChange(role.Id, tenant.id)}
    />
  ));

  return (
    <Card border="success" className="my-3">
      <Card.Header className="text-center">{tenant.Name}</Card.Header>
      <Card.Body>
        <Form>
          <Form.Check type="radio" custom>
            <Col>{roleItems}</Col>
          </Form.Check>
        </Form>
      </Card.Body>
    </Card>
  );
};

export default TenantItem;
