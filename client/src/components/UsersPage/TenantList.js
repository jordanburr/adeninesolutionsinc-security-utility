import React, { useState, useEffect } from "react";
import axios from "axios";
import TenantItem from "./TenantItem";

// import react-toastify dependencies
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

toast.configure();

let tenantApi = "http://localhost:5000/api/user-security/tenants/";
const ROLES_API = "http://localhost:5000/api/user-security/roles/";

const TenantList = (props) => {
  const { userId } = props;

  const [tenants, setTenants] = useState([]);
  const [roles, setRoles] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    fetchData();
    fetchRoles();
  }, [userId]);

  const fetchData = async () => {
    setLoading(true);
    await axios
      .get(tenantApi + userId, {
        headers: { token: sessionStorage.getItem("token") },
      })
      .then((res) => {
        setTenants(res.data);
      })
      .catch((error) => console.log(error));
    setLoading(false);
  };

  const fetchRoles = async () => {
    setLoading(true);
    await axios
      .get(ROLES_API, {
        headers: { token: sessionStorage.getItem("token") },
      })
      .then((res) => {
        setRoles(res.data);
      })
      .catch((error) => console.log(error));
    setLoading(false);
  };

  const handleRoleChange = async (userRoleId, tenantId) => {
    const body = {
      id: tenantId,
      userRoleId: userRoleId,
    };
    await axios
      .put(tenantApi + userId, body, {
        headers: { token: sessionStorage.getItem("token") },
      })
      .then((res) => {
        toast.success("User role was updated!", {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      })
      .catch((error) => {
        toast.error("User could NOT be updated!", {
          position: "bottom-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        console.log(error);
      });
  };

  const tenantItems = tenants.map((tenant) => (
    <TenantItem
      key={`${userId}-${tenant.id}`}
      tenant={tenant}
      userId={userId}
      roleId={tenant.UserRole_id}
      currentRole={tenant.RoleName}
      handleRoleChange={handleRoleChange}
      roles={roles}
    />
  ));

  return (
    <div className="container">
      {loading ? (
        <span
          className="spinner-border align-items-center"
          role="status"
          aria-hidden="true"
        ></span>
      ) : (
        <div>{tenantItems}</div>
      )}
    </div>
  );
};

export default TenantList;
