import React, { Fragment } from "react";
import { Button } from "react-bootstrap";

// get our fontawesome imports
import { faCog } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const UserItem = (props) => {
  const { user, handleUserSelect, isSelected } = props;

  return (
    <Fragment>
      <tr className={isSelected ? "table-success" : null}>
        <td>
          {user.FirstName} {user.LastName}
        </td>
        <td>{user.Email}</td>
        <td align="center">
          <Button onClick={() => handleUserSelect(user)}>
            <FontAwesomeIcon icon={faCog} />
          </Button>
        </td>
      </tr>
    </Fragment>
  );
};

export default UserItem;
