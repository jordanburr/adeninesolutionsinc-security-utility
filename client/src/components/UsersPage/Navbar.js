import React, { useContext } from "react";
import { AuthContext } from "../../context/Auth";
import { Link } from "react-router-dom";

// get our fontawesome imports
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavDropdown, Navbar as BootstrapNavbar, Nav } from "react-bootstrap";

const Navbar = () => {
  const { toggleAuthentication, user } = useContext(AuthContext);

  const handleLogout = () => {
    toggleAuthentication();
  };

  return (
    <div className="container-fluid mb-2">
      <div className="row justify-content-center">
        <BootstrapNavbar expanded className="my-1 static-top">
          <BootstrapNavbar.Brand>
            <img
              src={require("../../images/Logo.png")}
              alt=""
              className="img-fluid"
            />
          </BootstrapNavbar.Brand>
          <BootstrapNavbar.Brand>
            <h1 style={{ fontFamily: "Century Gothic" }}>
              RetainBi User Security Management
            </h1>
          </BootstrapNavbar.Brand>
          <BootstrapNavbar.Toggle aria-controls="navbar-nav" />
          <BootstrapNavbar.Collapse id="navbar-nav">
            <Nav className="mr-auto">
              <NavDropdown title={<FontAwesomeIcon icon={faUser} size="2x" />}>
                <NavDropdown.Item onClick={handleLogout}>Logout &#x21b3;</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </BootstrapNavbar.Collapse>
        </BootstrapNavbar>
      </div>
    </div>
  );
};

export default Navbar;
